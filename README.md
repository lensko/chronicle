# Chronicle

## About

Chronicle is a proof of concept extract and load framework for Databricks.

The aim of the framework is to use the components already available in Databricks along with third party drivers
to quickly and easily move data from sources to delta history tables, while also performing change data capture.

Currently the framework uses Spark, Hive catalog, Delta tables and optionally Databricks secrets.

The intent is to allow anyone to use the framework as is or modify it for their own purposes, without any guarantees.

## Overview

The framework supports reading data from source and writing directly to SCD2 style delta tables without first staging the data.

The framework supports using parallel queries and splitting data into multiple batches to deal with large data, as long as the columns used for this does not contain null values.

So far the framework supports reading from SqlServer, MySql and PostgreSql but it should be very straight forward to add additional databases that are supported by spark.
Links to the drivers can be found as comments directly in the source code for the reader classes.

The framework is opinionated about column naming and will rename source columns to snake_case while using snake_case with a leading underscore for its own metadata.
When using the API / facade functions, all refrences to column names should match the source, as the framework will handle any needed comparisons between source and target.

The API / facade functions are designed to be idempotent, so unless source data has changed in the meantime, additional runs beyond the first will not produce any further changes.

## Architechture

The framework so far consists of four catagories of components.

- Readers which are responsible for preparing data frames containing source data.
- Writers which are responsible for taking a source data frame and writing a delta table.
- Helper functions which provide various functionality that isn't directly tied to any class.
- API / facade functions which coordinate the other components so common extract and load operations can be performed with a single function call.

Note that writers can be used with any source data frame so extending and prototyping can be done with any Databricks notebook as long as it produces a data frame.

In the future there will probably be a couple of more catagories of components, depending on how new features are implemented.

## Testing

The framework has only been sporadically tested. So far the focus has been on design and basic features.

## Feature ideas

Since the framework is currently only a proof of concept the focus has been on covering the usual batch use cases.
However there are many other features that would be fun to add when time allows for it.

- Support for excluding source columns, renaming source columns and/or injecting custom transformations between the readers and writers.
- More logical readers to support reading from more databases, delta staging tables and other sources.
- Logical writer for staging data to support more complex flows.
- Logical writer for writing snapshots of data lacking proper keys.
- Logical writer for writing append only data to to support simpler data flows for sources that does not need checksum or operation metadata.
- Logical writer for archiving streams that have the same metadata and are interchangeable with the batch append writer.
- Add support for using partitions.
- Add Dockerfile and required scripts for building PySpark wheel version of the framework.
- Test/Tune jdbc fetch size. 100-2000?

## Try it out

- Copy chronicle.py into your Databricks workspace.
- Copy the jdbc drivers you want into your Databricks workspace. Links to drivers are inside chronicle.py. Check out this article if you are unfamiliar with the process: https://medium.com/@wesmelton/how-to-use-jdbc-mysql-sql-drivers-with-databricks-apache-spark-774544aa856
- Once your cluster is running and the drivers are present in your workspace you can install them on the cluster itself using the library tab. This can later be automated through cluster configuration and job configuration once you have it working.
- Create a python notebook.
- Add a cell where you load the framework: %run /Shared/chronicle
- Add another cell where you call one of the API functions from the framework. See examples below or take a look at the bottom of the chronicle.py file.
- If you want to use secrets scopes you also have to prepare those.

### Load full example 1
```
reader = {
    "class_name"  : "PostgresqlReader",
    "host"        : "myserver.com",
    "port"        : "1001",
    "database"    : "test",
    "username"    : "postgres",
    "password"    : "mypassword"
}

load_full(
    reader = reader,
    source = "store",
    key    = "store_id",
    target = "archive.store",
)
```

### Load full example 2
```
load_full(
    reader          = reader,
    source          = "product",
    key             = "product_id",
    target          = "archive.product",
    parallel_number = None,
    parallel_column = None
)
```

### Load incremetal example 1
```
load_incremental(
    reader          = reader,
    source          = "`order`",       # Since the name of the table is also a keyword MySql requires backticks around it.
    key             = "order_id",
    target          = "archive.order",
    bookmark_column = "update_time",
    bookmark_offset = "2 minutes",     # Incremental read overlaps by 2 minutes to make sure we dont miss any updates.
    parallel_number = None,
    parallel_column = None
)
```

### Load incremetal example 2
```
load_incremental(
    reader          = reader,
    source          = "transaction",
    key             = "transaction_id",
    target          = "archive.transaction",
    bookmark_column = "transaction_date",
    bookmark_offset = "5 days",              # We re-read transactions from the previous 5 days to look for updated information.
    parallel_number = 4,                     # Since the table is big we use 4 parallel queries to read the data.
    parallel_column = "transaction_id"       # This column must not contain any NULLs.
)
```

### Load deleted example 1
```
load_deleted(
    reader = reader,
    source = "transaction",        # Altough the table is big and we only want to load data incrementally we can run a separate check for deleted records.
    key    = "transaction_id",     # Only the key column(s) will be retrieved from the source.
    target = "archive.transaction"
)
```

### Load history example 1
```
load_history(
    reader          = reader,
    source          = "sales",
    target          = "archive.sales",
    key             = "sales_id",
    batch_column    = "sales_date",    # This column is also used to resume if the load gets stopped or interrupted before all dates have been loaded.
    parallel_number = 4,
    parallel_column = "sales_id"
)
```
