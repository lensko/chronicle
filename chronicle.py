# Databricks notebook source
from re import match, sub
from pyspark.sql.functions import col, concat_ws, current_timestamp, expr, lag, lead, lit, md5, row_number, when
from pyspark.sql.window import Window

# Perform additional initialization if framework is imported as module.
if __name__ != "__main__":
    from pyspark.dbutils import DBUtils
    from pyspark.sql import SparkSession
    spark = SparkSession.builder.getOrCreate()
    dbutils = DBUtils(spark)


# Return new reader instance if argument is string or dictonary, else return argument itself.
def get_reader(reader):
    # Retrieve dictionary containing data source configuration if reader is string.
    if isinstance(reader, str):
        reader = get_data_source_configuration(reader)
    # Create new instance if reader is dictionary.
    if isinstance(reader, dict):
        class_name = reader["class_name"]
        if class_name in ["MysqlReader", "PostgresqlReader", "SqlserverReader"]:
            class_name = globals()[class_name]
            reader = class_name(**reader)
        else:
            raise Exception("Invalid reader class")
    # Return either newly created reader instance or initial argument.
    return reader

# Validate mode against dynamic list of values.
def validate_mode(valid, mode):
    if mode not in valid:
        raise Exception("Invalid mode")

# Optionally filter data frame.
def filter(df, exclude=None, where=None):
    if where is not None:
        df = df.where(where)
    if exclude is not None:
        df = exclude_columns(df, exclude)
    return df

# Exclude columns from data frame.
def exclude_columns(df, exclude):
    if isinstance(exclude, str):
        return df.drop(exclude)
    elif isinstance(exclude, list):
        return df.drop(*exclude)
    else:
        raise Exception("Invalid exclude")

# Add _checksum column to beginning of data frame.
def add_checksum_column(df, ignore=None):
    if ignore is None:
        return df.select([md5(concat_ws("<|^|>", *sorted(df.columns))).alias("_checksum"), "*"])
    elif isinstance(ignore, str):
        ignore = conform_column_name(ignore)
        return df.select([md5(concat_ws("<|^|>", *sorted(c for c in df.columns if c != ignore))).alias("_checksum"), "*"])
    elif isinstance(ignore, list):
        ignore = [conform_column_name(c) for c in ignore]
        return df.select([md5(concat_ws("<|^|>", *sorted(c for c in df.columns if c not in ignore))).alias("_checksum"), "*"])
    else:
        raise Exception("Invalid ignore")

# Add _key column to beginning of data frame.
def add_key_column(df, key):
    if isinstance(key, str):
        return df.select([df[conform_column_name(key)].alias("_key"), "*"])
    elif isinstance(key, list):
        key = sorted([conform_column_name(c) for c in key])
        return df.select([concat_ws("-", *key).alias("_key"), "*"])
    else:
        raise Exception("Invalid key")

# Conform data frame column names to naming convention and check for duplicate column names.
def conform_column_names(df):
    df = df.toDF(*[conform_column_name(c) for c in df.columns])
    duplicates = {c for c in df.columns if df.columns.count(c) > 1}
    if duplicates:
        raise Exception(f"Duplicate column name(s): {duplicates}")
    return df

# Conform column name to naming convention.
def conform_column_name(cn):
    cn = cn.lower()                                # Convert to lowercase.
    cn = cn.translate(str.maketrans("æøå", "eoa")) # Replace Norwegian characters with Latin characters.
    cn = sub("[^a-z0-9]+", "_", cn)                # Replace all characters except letters and numbers with underscores.
    cn = sub("^_|_$", "", cn)                      # Remove leading and trailing underscores.
    return cn

# Return the current maximum value from the specified Delta table and column, optionally adjusted by offset.
def get_max(table, column, offset=None):
    # Return early if table does not exist.
    if not spark.catalog._jcatalog.tableExists(table):
        return None
    # Prepare query.
    column = conform_column_name(column)
    if offset is None:
        query = f"SELECT MAX({column}) FROM {table}"
    else:
        if (isinstance(offset, int) and not isinstance(offset, bool)) or (isinstance(offset, str) and match("^[0-9]+$", offset)):
            query = f"SELECT MAX({column}) - {offset} FROM {table}"
        elif isinstance(offset, str):
            query = f"SELECT MAX({column}) - INTERVAL {offset} FROM {table}"
        else:
            raise Exception("Invalid offset")
    # Execute query.
    return spark.sql(query).collect()[0][0]

# Return number of output rows from the last operation performed against the specified Delta table.
def get_num_output_rows(table):
    return spark.sql(f"DESCRIBE HISTORY {table} LIMIT 1").collect()[0][12]["numOutputRows"]

# Read from Delta table and return data frame containing current rows that have not been marked as deleted.
def read_active(table, select=None):
    wd = Window.partitionBy(col("_key")).orderBy(col("_timestamp").desc())
    df = spark.table(table).withColumn("_rn", row_number().over(wd)) \
        .where(col("_rn") == 1).drop(col("_rn")) \
        .where(col("_operation") != "D")
    if select is None:
        return df
    elif isinstance(select, list) or isinstance(select, str):
        return df.selectExpr(select)
    else:
        raise Exception("Invalid select")


# Delta change data capture writer.
class DeltaChangeWriter:

    # Initialize writer.
    def __init__(self, mode, table, key, ignore=None):
        self.key    = key
        self.mode   = mode
        self.table  = table
        self.ignore = ignore

    # Write data frame to Delta table.
    def write(self, df):
        df = conform_column_names(df)
        mode = self.mode
        if mode == "d":
            return self.__delete(df)
        if not spark.catalog._jcatalog.tableExists(self.table):
            self.__create_table(df)
            mode = "a"
        if mode == "a" or mode == "ka":
            return self.__append(df)
        if mode == "i":
            return self.__insert(df)
        if mode == "iu":
            return self.__insert_update(df)
        if mode == "iud":
            return self.__insert_update_delete(df)
        raise Exception("Invalid mode")

    # First add checksum column followed by key column.
    def __add_checksum_and_key_columns(self, df):
        if self.mode == "ka": # Keyless append.
            return df
        df = add_checksum_column(df, self.ignore)
        df = add_key_column(df, self.key)
        return df

    # Create Delta table with metadata columns _operation, _timestamp, _key and _checksum.
    def __create_table(self, df):
        df = self.__add_checksum_and_key_columns(df)
        df = df.select([current_timestamp().alias("_timestamp"), "*"])
        df = df.select([lit("I").alias("_operation"), "*"])
        df.filter("1=0").write.format("delta") \
            .option("delta.autoOptimize.optimizeWrite", "true") \
            .option("delta.autoOptimize.autoCompact", "true") \
            .saveAsTable(self.table)

    # Append source data frame.
    def __append(self, df):                  
        df = self.__add_checksum_and_key_columns(df)
        df = df.select([current_timestamp().alias("_timestamp"), "*"])
        df = df.select([lit("I").alias("_operation"), "*"])
        df.write.format("delta").mode("append").saveAsTable(self.table)

    # Capture inserted records from full or partial source data frame.
    def __insert(self, df):
        df = self.__add_checksum_and_key_columns(df).alias("s") \
            .join(read_active(self.table, "_key").alias("a"), "_key", "anti") \
            .withColumn("_operation", lit("I")) \
            .withColumn("_timestamp", current_timestamp()) \
            .select("_operation", "_timestamp", "s.*")
        df.write.format("delta").mode("append").saveAsTable(self.table)

    # Capture inserted and updated records from full or partial source data frame.
    def __insert_update(self, df):
        df = self.__add_checksum_and_key_columns(df).alias("s") \
            .join(read_active(self.table, ["_key", "_checksum"]).alias("a"), "_key", "left") \
            .withColumn("_operation", expr("CASE WHEN a._key IS NULL THEN 'I' ELSE 'U' END")) \
            .withColumn("_timestamp", current_timestamp()) \
            .where("(a._key IS NULL) OR (a._checksum != s._checksum)") \
            .select("_operation", "_timestamp", "s.*")
        df.write.format("delta").mode("append").saveAsTable(self.table)

    # Capture inserted, updated and deleted records from full source data frame.
    def __insert_update_delete(self, df):
        wd = Window.partitionBy(col("_key")).orderBy(col("_origin"))
        df = self.__add_checksum_and_key_columns(df).withColumn("_origin", lit(1)) \
        .union(read_active(self.table, "* EXCEPT (_operation, _timestamp)").withColumn("_origin", lit(2))) \
        .select([
            when((col("_origin") == 1) & (lead(col("_origin")).over(wd).isNull()), "I") \
            .when((col("_origin") == 1) & (lead(col("_checksum")).over(wd) != col("_checksum")), "U") \
            .when((col("_origin") == 2) & (lag(col("_origin")).over(wd).isNull()), "D") \
            .alias("_operation"),
            current_timestamp().alias("_timestamp"),
            "*"
        ]).drop(col("_origin")).where(col("_operation").isNotNull())
        df.write.format("delta").mode("append").saveAsTable(self.table)

    # Capture deleted records from full or key only source data frame.
    def __delete(self, df):
        df = read_active(self.table, "* EXCEPT (_operation, _timestamp)").alias("a") \
            .join(add_key_column(df, self.key).alias("s"), "_key", "anti") \
            .withColumn("_operation", lit("D")) \
            .withColumn("_timestamp", current_timestamp()) \
            .select("_operation", "_timestamp", "a.*")
        df.write.format("delta").mode("append").saveAsTable(self.table)


# Base JDBC reader.
class BaseJdbcReader:

    # Read from table using single query.
    def __read_single(self, table):
        return spark.read.jdbc(properties=self.properties, url=self.url, table=table, numPartitions=1)

    # Read from table using multiple parallel queries.
    def __read_parallel(self, table, parallel_column, parallel_number, lower_bound, upper_bound):
        return spark.read.jdbc(
            properties    = self.properties,
            url           = self.url,
            table         = table,
            numPartitions = parallel_number,
            column        = parallel_column,
            lowerBound    = lower_bound,
            upperBound    = upper_bound
        )

    # Execute query and return data frame.
    def query(self, query):
        return self.__read_single(f"({query}) AS q")

    # Read from table and return data frame containing only key columns.
    def read_key(self, table, key, where=None):
        if isinstance(key, str):
            df = self.__read_single(table).select(key)
        elif isinstance(key, list):
            df = self.__read_single(table).select(*key)
        else:
            raise Exception("Invalid key")
        return filter(df=df, where=where)

    # Read from table and return data frame.
    def read(self, table, exclude=None, where=None, parallel_column=None, parallel_number=None):
        if parallel_column is None and parallel_number is None:
            df = self.__read_single(table)
        elif parallel_column is not None and parallel_number is not None:
            bounds = self.get_bounds(table=table, parallel_column=parallel_column, where=where)
            df = self.__read_parallel(
                table           = table,
                parallel_column = parallel_column,
                parallel_number = parallel_number,
                lower_bound     = bounds["l"],
                upper_bound     = bounds["u"]
            )
        else:
            raise Exception("Invalid parallel_column and parallel_number combination")
        return filter(df=df, exclude=exclude, where=where)

    # Read from table and return data frame containing rows where the column value is equal to the provided value.
    def read_equal_to(self, table, column, value, exclude=None, where=None, parallel_column=None, parallel_number=None):
        if parallel_column is None and parallel_number is None:
            df = self.__read_single(table)
        elif parallel_column is not None and parallel_number is not None:
            bounds = self.get_bounds_equal_to(table=table, column=column, value=value, parallel_column=parallel_column, where=where)
            df = self.__read_parallel(
                table           = table,
                parallel_column = parallel_column,
                parallel_number = parallel_number,
                lower_bound     = bounds["l"],
                upper_bound     = bounds["u"]
            )
        else:
            raise Exception("Invalid parallel_column and parallel_number combination")
        return filter(df=df.where(col(column) == value), exclude=exclude, where=where)

    # Read from table and return data frame containing rows where the column value is greater than the provided value.
    def read_greater_than(self, table, column, value=None, exclude=None, where=None, parallel_column=None, parallel_number=None):
        # Fall back to normal read if value is None so incremental loads can be initalized.
        if value is None:
            return self.read(table=table, where=where, parallel_column=parallel_column, parallel_number=parallel_number)
        else:
            if parallel_column is None and parallel_number is None:
                df = self.__read_single(table)
            elif parallel_column is not None and parallel_number is not None:
                bounds = self.get_bounds_greater_than(table=table, column=column, value=value, parallel_column=parallel_column, where=where)
                df = self.__read_parallel(
                    table           = table,
                    parallel_column = parallel_column,
                    parallel_number = parallel_number,
                    lower_bound     = bounds["l"],
                    upper_bound     = bounds["u"]
                )
            else:
                raise Exception("Invalid parallel_column and parallel_number combination")
            return filter(df=df.where(col(column) > value), exclude=exclude, where=where)

    # Get bounds for parallel read.
    def get_bounds(self, table, parallel_column, where=None):
        df = self.query(f"SELECT MIN({parallel_column}) AS l, MAX({parallel_column}) AS u FROM {table} WHERE ({self.substitute(where)})")
        return df.toPandas().to_dict(orient="records")[0]

    # Get bounds for parallel equal to read.
    def get_bounds_equal_to(self, table, column, value, parallel_column, where=None):
        df = self.query(f"SELECT MIN({parallel_column}) AS l, MAX({parallel_column}) AS u FROM {table} WHERE ({self.substitute(where)}) AND ({column} = {self.quote(value)})")
        return df.toPandas().to_dict(orient="records")[0]

    # Get bounds for parallel greater than read.
    def get_bounds_greater_than(self, table, column, value, parallel_column, where=None):
        df = self.query(f"SELECT MIN({parallel_column}) AS l, MAX({parallel_column}) AS u FROM {table} WHERE ({self.substitute(where)}) AND ({column} > {self.quote(value)})")
        return df.toPandas().to_dict(orient="records")[0]

    # Return list of distinct values from the specified table and column, optionally filtered by greater_than and where.
    def get_distinct(self, table, column, greater_than=None, where=None):
        if greater_than is None:
            query = f"SELECT DISTINCT {column} AS d FROM {table} WHERE ({self.substitute(where)}) ORDER BY {column}"
        else:
            query = f"SELECT DISTINCT {column} AS d FROM {table} WHERE ({self.substitute(where)}) AND ({column} > {self.quote(greater_than)}) ORDER BY {column}"
        if self.__class__.__name__ == "SqlserverReader":
            query += " OFFSET 0 ROWS"
        return list(self.query(query).toPandas()['d'])

    # Substitute where with 1=1 if where is not present.
    def substitute(self, where):
        if where is None:
            where = "1=1"
        return where

    # Quote value so it can be used as part of sql statement.
    def quote(self, value):
        if value.__class__.__name__ in ["date", "datetime", "str", "Timestamp"]:
            value = f"'{value}'"
        return value


# MySQL JDBC reader.
# https://dev.mysql.com/downloads/connector/j/
class MysqlReader(BaseJdbcReader):

    # Initialize reader.
    def __init__(self, host, port, database, username, password, **kwargs):
        self.url = f"jdbc:mysql://{host}:{port}/{database}?user={username}&password={password}"
        self.properties = {"driver": "com.mysql.jdbc.Driver"}


# PostgreSQL JDBC reader.
# https://jdbc.postgresql.org/download.html
class PostgresqlReader(BaseJdbcReader):

    # Initialize reader.
    def __init__(self, host, port, database, username, password, **kwargs):
        self.url = f"jdbc:postgresql://{host}:{port}/{database}?user={username}&password={password}"
        self.properties = {"driver": "org.postgresql.Driver"}


# SQLServer JDBC reader.
# https://docs.microsoft.com/en-us/sql/connect/jdbc/download-microsoft-jdbc-driver-for-sql-server?view=sql-server-ver15
class SqlserverReader(BaseJdbcReader):

    # Initialize reader.
    def __init__(self, host, port, database, username, password, **kwargs):
        self.url = f"jdbc:sqlserver://{host}:{port};databaseName={database}"
        self.properties = {
            "driver"   : "com.microsoft.sqlserver.jdbc.SQLServerDriver",
            "user"     : username,
            "password" : password
        }


# Perform full load.
def load_full(reader, source, target, key, exclude=None, ignore=None, where=None, mode="iud", parallel_number=None, parallel_column=None):
    validate_mode(valid=["iud", "iu", "i"], mode=mode)
    reader = get_reader(reader=reader)
    writer = DeltaChangeWriter(mode=mode, table=target, key=key, ignore=ignore)
    df = reader.read(table=source, exclude=exclude, where=where, parallel_number=parallel_number, parallel_column=parallel_column)
    writer.write(df)
    print(f"Result rows: {get_num_output_rows(target)}")

# Perform overlapping incremental load.
def load_overlapping(reader, source, target, key, bookmark_column, bookmark_offset=None, exclude=None, ignore=None, where=None, mode="iu", parallel_number=None, parallel_column=None):
    validate_mode(valid=["iu", "i"], mode=mode)
    reader = get_reader(reader=reader)
    writer = DeltaChangeWriter(mode=mode, table=target, key=key, ignore=ignore)
    max = get_max(table=target, column=bookmark_column, offset=bookmark_offset)
    df = reader.read_greater_than(table=source, column=bookmark_column, value=max, exclude=exclude, where=where, parallel_number=parallel_number, parallel_column=parallel_column)
    writer.write(df)
    print(f"Result rows: {get_num_output_rows(target)}")

# Perform append only incremental load.
def load_appended(reader, source, target, key, bookmark_column, exclude=None, ignore=None, where=None, mode="a", parallel_number=None, parallel_column=None):
    validate_mode(valid=["a", "ka"], mode=mode)
    reader = get_reader(reader=reader)
    writer = DeltaChangeWriter(mode=mode, table=target, key=key, ignore=ignore)
    max = get_max(table=target, column=bookmark_column)
    df = reader.read_greater_than(table=source, column=bookmark_column, value=max, exclude=exclude, where=where, parallel_number=parallel_number, parallel_column=parallel_column)
    writer.write(df)
    print(f"Result rows: {get_num_output_rows(target)}")

# Capture deleted records by comparing source and target keys.
def load_deleted(reader, source, target, key, where=None):
    reader = get_reader(reader=reader)
    writer = DeltaChangeWriter(mode="d", table=target, key=key)
    df = reader.read_key(table=source, key=key, where=where)
    writer.write(df)
    print(f"Result rows: {get_num_output_rows(target)}")

# Perform or continue full backfill by splitting source data into multiple batches and appending one batch at a time.
def backfill_full(reader, source, target, key, backfill_column, exclude=None, ignore=None, where=None, mode="a", parallel_number=None, parallel_column=None):
    validate_mode(valid=["a", "ka"], mode=mode)
    reader = get_reader(reader=reader)
    writer = DeltaChangeWriter(mode=mode, table=target, key=key, ignore=ignore)
    max = get_max(table=target, column=backfill_column)
    distinct = reader.get_distinct(table=source, column=backfill_column, greater_than=max, where=where)
    print(f"Batch count: {len(distinct)}")
    for value in distinct:
        print(f"Batch value: {value}")
        df = reader.read_equal_to(table=source, column=backfill_column, value=value, exclude=exclude, where=where, parallel_number=parallel_number, parallel_column=parallel_column)
        writer.write(df)
        print(f"Result rows: {get_num_output_rows(target)}")
